const name = process.env.VUE_APP_TITLE || '数字物流平台' // 网页标题
const port = process.env.port || process.env.npm_config_port || 80 // 端口

function resolve(dir) {
  return path.join(__dirname, dir)
}

const { defineConfig } = require('@vue/cli-service')
const path = require("path");
module.exports = defineConfig({
  transpileDependencies: true,
  // 部署生产环境和开发环境下的URL。
  // 默认情况下，Vue CLI 会假设你的应用是被部署在一个域名的根路径上
  // 例如 https://www.ruoyi.vip/。如果应用被部署在一个子路径上，你就需要用这个选项指定这个子路径。例如，如果你的应用被部署在 https://www.ruoyi.vip/admin/，则设置 baseUrl 为 /admin/。
  publicPath: process.env.NODE_ENV === "production" ? "/" : "/",
  // 在npm run build 或 yarn build 时 ，生成文件的目录名称（要和baseUrl的生产环境路径一致）（默认dist 打包生成的目录名称）
  outputDir: 'dist',
  // 用于放置生成的静态资源 (js、css、img、fonts) 的；（项目打包之后，静态资源会放在这个文件夹下）
  assetsDir: 'static',
  // 是否开启eslint保存检测，有效值：ture | false | 'error'
  lintOnSave: process.env.NODE_ENV === 'development',
  // 如果你不需要生产环境的 source map，可以将其设置为 false 以加速生产环境构建。
  productionSourceMap: false,
  // webpack-dev-server 相关配置(指向开发环境 API 服务器)
  devServer: {
    host: '0.0.0.0',
    port: port,
    open: true,
    // 多代理实现
    proxy: {
      // detail: https://cli.vuejs.org/config/#devserver-proxy
      // 代理地址1 - 销售演示系统 当接口方法process.env.VUE_APP_BASE_API对应的字段值时 则代理到以下路径
      [process.env.VUE_APP_BASE_API]: {
        // 万和
        target:'https://bops.hebeiwanhewl.com/api/',

        // 销售演示
        // target:'http://bops.demo.wlhyos.com/api/',
        // target:'https://anglelover.eicp.net/api/',
        // target: `http://localhost:8080`,
        // target: `http://192.168.12.197:3001/business-xuanleoa-service/`,
        // target: `https://y771823r62.zicp.fun/`,
        // target: `http://106.15.190.131:8100/prod-api/`,
        // target: `https://dlp.kaiyun168.cn/prod-api/`,
        // `changeOrigin`表示是否改变请求头中的`Host`字段
        changeOrigin: true,
        // `pathRewrite`表示重写路径。
        pathRewrite: {
          ['^' + process.env.VUE_APP_BASE_API]: ''
        }
      },
<<<<<<< HEAD
=======
      // 代理地址2 - 开运系统
      [process.env.VUE_APP_BASE_KY_API]: {
        target: `http://106.15.190.131:8100/prod-api/`,
        changeOrigin: true,
        pathRewrite: {
          ['^' + process.env.VUE_APP_BASE_KY_API]: ''
        }
      },
>>>>>>> 907a94c99d100dcf2e78f27e0ac947f2f5665db7
      // 代理地址3 - 测试
      [process.env.VUE_APP_BASE_TEST_API]: {
        target: `http://118.190.151.58:5838/`,
        changeOrigin: true,
        pathRewrite: {
          ['^' + process.env.VUE_APP_BASE_TEST_API]: ''
        }
      },
<<<<<<< HEAD
=======

>>>>>>> 907a94c99d100dcf2e78f27e0ac947f2f5665db7
    },
    allowedHosts:'all'
  },
  chainWebpack: (config) => {

    // 配置 svg-sprite-loader
    config.module
        .rule('svg')
        .exclude.add(resolve('src/assets/icons'))
        .end()
    config.module
        .rule('icons')
        .test(/\.svg$/)
        .include.add(resolve('src/assets/icons'))
        .end()
        .use('svg-sprite-loader')
        .loader('svg-sprite-loader')
        .options({
          symbolId: 'icon-[name]'
        })
        .end()
  },
})
