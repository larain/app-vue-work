(function () {
  // 在配置文件中最好套一层立即执行函数，确保这个配置文件是在最开始执行的
  // 将配置信息放在window对象上,使其变成全局都可以访问的
  window.configs = {
    // 是否访问web工程
    isWeb: false,
  }
})();
