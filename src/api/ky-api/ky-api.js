import request from "@/utils/request-ky";

// 获取验证码
export function getCodeImg() {
  return request({
    url: '/captchaImage',
    headers: {
      isToken: false
    },
    method: 'get',
    timeout: 20000
  })
}


/**
 * 导入快货运运单数据
 * @param data
 * @returns {*}
 */
export function uploadKhyWaybill(data) {
  return request({
    url: '/third/khyWaybill/add',
    method: 'post',
    data,
  })
}

/**
 * 导入快货运司机数据
 * @param data
 * @returns {*}
 */
export function uploadKhyDriver(data) {
  return request({
    url: '/third/khyDriver/batchAdd',
    method: 'post',
    data,
  })
}

/**
 * 导入快货运车辆数据
 * @param data
 * @returns {*}
 */
export function uploadKhyCarInfo(data) {
  return request({
    url: '/third/khyVehicle/batchAdd',
    method: 'post',
    data,
  })
}

/**
 * 导入快货运收款账户数据
 * @param data
 * @returns {*}
 */
export function uploadKhyPayeeInfo(data) {
  return request({
    url: '/third/khyPayee/batchAdd',
    method: 'post',
    data,
  })
}

