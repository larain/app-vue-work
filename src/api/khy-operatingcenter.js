import request from "@/utils/request";

/**
 * 获取司机资料列表数据
 * @param data
 * @returns {*}
 */
export function getDriverInfo(data) {
  return request({
    url: '/resource/platform/driver/searchDriverList',
    method: 'post',
    data,
  })
}


/**
 * 获取司机图片信息
 * @param data
 * @returns {*}
 */
export function getDriverImageInfo(data) {
  return request({
    url: '/basic-ability/file/getThumbnailFileTemporaryUrlList',
    method: 'post',
    data,
  })
}

/**
 * 获取车辆资料列表数据
 * @param data
 * @returns {*}
 */
export function getCarInfo(data) {
  return request({
    url: '/resource/platform/truck/platSearchTruckPage',
    method: 'post',
    data,
  })
}

/**
 * 获取运单查询数据
 * @param data
 * @returns {*}
 */
export function getWaybillSearch(data) {
  return request({
    url: '/transport-center/transport/bill/platform/searchPageListByCondition',
    method: 'post',
    data,
  })
}

/**
 * 获取合同查询数据 - 合同管理
 * @param data
 * @returns {*}
 */
export function getContractSearch(data) {
  return request({
    url: '/resource/platform/contract/carrier/search',
    method: 'post',
    data,
  })
}

/**
 * 获取收款人信息数据
 * @param data
 * @returns {*}
 */
export function getPayeeInfo(data) {
  return request({
    url: '/resource/platform/payee/searchPlatformPayeeList',
    method: 'post',
    data,
  })
}

/**
 * 获取支付明细表
 * @param data
 * @returns {*}
 */
export function getPayDetailInfoInfo(data) {
  return request({
    url: '/transport-center/finance/pay/searchPayDetailPage',
    method: 'post',
    data,
  })
}

