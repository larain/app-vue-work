import request from "@/utils/request";
export function login(username, password, uuid) {
  const data = {
    loginName:username,
    password,
    imei:uuid,
  }
  return request({
    url: '/app/login/doLogin',
    headers: {
      isToken: false
    },
    method: 'post',
    data,
  })
}

export function unbindAccount(data) {
  return request({
    url: '/admin/dept/unbindingImei',
    method: 'post',
    data,
  })
}

/**
 * 获取验证码
 * @param data
 * @returns {*}
 */
export function getCaptcha(data) {
  return request({
    url: '/api/account-platform/captcha/get',
    method: 'get',
    data,
  })
}

/**
 * 快货运系统登录校验账号密码验证码等信息 密码md5加密
 * @param data
 * @returns {*}
 */
export function oauthToken(data) {
  return request({
    url: '/account-platform/oauth/token',
    method: 'post',
    data,
  })
}

export function getTestCode(data) {
  return request({
    url: '/sys/randomImage/' + data,
    method: 'get',
  })
}

/**
 * 快货运系统账号密码校验完成 获取用户信息
 * @param data
 * @returns {*}
 */
export function getUserInfo(data) {
  return request({
    url: '/account-platform/user/getUserInfo',
    method: 'post',
    data,
  })
}

/**
 * 退出登录
 * @param data
 * @returns {*}
 */
export function logout(data) {
  return request({
    url: '/account-platform/auth/logout',
    method: 'post',
    data,
  })
}


