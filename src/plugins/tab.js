// 专门处理页面刷新 跳转 关闭
import store from '@/store'
import router from '@/router'

export default {
  /**
   * 关闭当前正在显示的页面 并开启新的页面
   * @param obj
   * @returns {Promise<NavigationFailure | void | undefined>}
   */
  closeOpenPage(obj) {
    store.dispatch("tagsView/delView", router.currentRoute);
    if (obj !== undefined) {
      return router.push(obj);
    }
  },
}
