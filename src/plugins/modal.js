import { ElMessage, ElMessageBox } from "element-plus";
export default {
  // 文本提示
  showText(text) {
    ElMessage(text)
  },

  /**
   * 确认编辑/删除弹窗
   * @param title
   * @param msg
   */
  showConfirmAlert(title, msg) {
    return ElMessageBox.confirm(
        msg,
        title,
        {
          confirmButtonText: '确定',
          cancelButtonText: '取消',
          type: 'warning',
        }
    )
  },
}
