import Vuex from 'vuex'
import app from './modules/app'
import user from './modules/user'
import permission from "@/store/modules/permission";
import settings from "@/store/modules/settings";
import tagsView from "@/store/modules/tagsView";


/**
 * 状态树：定义需要管理的数组、对象、字符 注意：状态管理器的值生命周期在页面未重新启动一致
 * @type {{userName: string}}
 */
const state = {
    // 客户名称 - 平台使用方名称 用以在导入数据到开运做区分
    customerName:'万合',
    projectName:'数字管理后台',
    // 列表分页数据最小以及最大一页
    pageSizeMin:500,
    pageSizeMax:1000,
}

/**
 * 更改store中state状态的唯一方法就是提交mutation(注意：是唯一)。每个mutation都有一个字符串类型的事件和一个回调函数，
 * 我们需要改变state的值就要在回调函数中改变。
 * 我们要执行这个回调函数，那么我们需要执行一个相应的调用方法：store.commit
 * @type {{SET_USERNAME(*, *): void}}
 */
const mutations = {
    // 外部调用 eg:store.commit('SET_PROJECT_NAME', '秋哥爱你')
    SET_PROJECT_NAME(state, projectName) {
        state.projectName = projectName
    },

    SET_CUSTOMER_NAME(state, customerName) {
        state.customerName = customerName
    }
}

/**
 * 类似计算属性 当需要从state的store中派生出一些状态 就需要使用getter getter会接受state作为第一个参数
 * 而getter的返回值会根据它的依赖缓存起来，只有getter中的依赖值发生改变才会重新计算
 * @type {{getUserName(*): string}}
 */
const getters = {
    // 获取时 可以做一些额外的操作 逻辑处理（测试代码）
    getUserName(state) {
        if (state.user.userName === '秋哥爱你') {
            return '祖国我爱你'
        }
        return state.user.userName
    },

    // 配置数据
    customerName: state => state.customerName,
    pageSizeMax: state => state.pageSizeMax,

    //  用户对象属性 - 此处是通过getters获取 派生的方法
    isLogin: state => state.user.isLogin,
    token: state => state.user.token,
    avatar: state => state.user.avatar,
    name: state => state.user.name,
    introduction: state => state.user.introduction,
    roles: state => state.user.roles,
    permissions: state => state.user.permissions,

    // 路由相关
    sidebarRoutes: state => state.permission.sidebarRoutes,
    staticRoutes: state => state.permission.staticRoutes,
    dynamicRoutes: state => state.permission.dynamicRoutes,
    activeMenu: state => state.permission.activeMenu,

    // 设置相关
    // 网页标题
    documentTitle: state => state.settings.documentTitle,

    // 路由入栈数据记录
    visitedViews: state => state.tagsView.visitedViews,
}

// 创建一个状态管理器实例
const store = new Vuex.Store({
    state,
    // 多模块
    modules:{
        app,
        user,
        permission,
        settings,
        tagsView
    },
    mutations,
    getters
})

// 导出状态管理器
export default store
