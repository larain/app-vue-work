/* eslint-disable */

const permission = {
    state:{
        // 侧边菜单栏数据源 - 菜单的路径 标题 图标 索引等信息(元素为对象 组件对应的为路径)
        sidebarRoutes:[],
        // 静态路由 - 链接到组件
        staticRoutes:[],
        // 动态路由 - 链接到组件
        dynamicRoutes:[],
        // 默认打开的菜单索引
        activeMenu:'0-0',
    },
    // Setter
    mutations:{
        SET_SIDEBAR_ROUTES:(state, sidebarRoutes) => {
            state.sidebarRoutes = sidebarRoutes
        },

        SET_STATIC_ROUTES:(state, staticRoutes) => {
            state.staticRoutes = staticRoutes
        },

        SET_DYNAMIC_ROUTES:(state, dynamicRoutes) => {
            state.dynamicRoutes = dynamicRoutes
        },

        SET_ACTIVE_MENU:(state, activeMenu) => {
            state.activeMenu = activeMenu
        }
    },
    actions:{
        // 生成路由
        GenerateRoutes({ commit }) {
            return new Promise((resolve) => {
                // 服务端下发的菜单栏数据 前端根据此数据转化为对应路由组件 并生成菜单栏最终数据
                /**
                 *
                 * @type {[{redirect: string, path: string, component: string, children: [{path: string, component: string, children: *[], meta: {activeMenu: string, icon: string, title: string}, name: string, index: string},{path: string, component: string, children: *[], meta: {activeMenu: string, icon: string, title: string}, name: string, index: string}], meta: {icon: string, title: string}, name: string, index: string},{redirect: string, path: string, component: string, children: [{path: string, component: string, children: *[], meta: {activeMenu: string, icon: string, title: string}, name: string, index: string}], meta: {icon: string, title: string}, name: string, index: string},{redirect: string, path: string, component: string, children: [{path: string, component: string, children: *[], meta: {activeMenu: string, icon: string, title: string}, name: string, index: string}], meta: {icon: string, title: string}, name: string, index: string}]}
                 */
                const appMenus = [
                    // {
                    //     path:'/user',
                    //     name:'User',
                    //     component:'layout',
                    //     redirect:'noRedirect',
                    //     meta: {
                    //         icon:'Setting',
                    //         title: '用户管理'
                    //     },
                    //     index:'1',
                    //     children: [
                    //         {
                    //             // 子路由的path会带上父路由的路径 组成完整路径
                    //             path:'user',
                    //             name:'WebUser',
                    //             component:'user/users',
                    //             meta: {
                    //                 icon:'User',
                    //                 title: '用户列表',
                    //                 // 对应选中的菜单索引
                    //                 activeMenu:'1-0',
                    //             },
                    //             index:'1-0',
                    //             children:[],
                    //         },
                    //         {
                    //             path:'roles',
                    //             name:'WebRoles',
                    //             component:'user/roles',
                    //             meta: {
                    //                 icon:'User',
                    //                 title: '角色列表',
                    //                 activeMenu:'1-1',
                    //             },
                    //             index:'1-1',
                    //             children:[],
                    //         },
                    //     ]
                    // },

                    // {
                    //     path:'/sysSetting',
                    //     name:'SysSetting',
                    //     component: "layout",
                    //     redirect: "noRedirect",
                    //     alwaysShow:true,
                    //     meta:{
                    //         icon:'Setting',
                    //         title:'系统设置',
                    //     },
                    //     index:'3-1',
                    //     children: [
                    //         {
                    //             path:'formSetting',
                    //             name:'SysSetting',
                    //             component: 'sysSetting',
                    //             meta:{
                    //                 icon:'EditPen',
                    //                 title: '表单设置',
                    //                 activeMenu:'3-1',
                    //             },
                    //             index:'3-1',
                    //             children:[],
                    //         }
                    //     ]
                    // },
                    // 运营中心
                    {
                        path:'/index',
                        name:'Index',
                        component:'layout',
                        redirect:'noRedirect',
                        meta:{
                            icon:'Histogram',
                            title:'运营中心',
                        },
                        index:'0-0',
                        // 即使子路由只有一个 也会嵌套显示
                        alwaysShow:true,
                        hidden:false,
                        children:[
                            {
                                path:'/index',
                                name:'WaybillSearch',
                                component:'khy-operate-center/waybill-search',
                                meta:{
                                    icon:"HomeFilled",
                                    title:'运单查询',
                                    activeMenu:'0-0',
                                    noCache:false,
                                },
                                index:'0-0',
                                children:[],
                            },
                            {
                                path:'driver-info',
                                name:'DriverInfo',
                                component:'khy-operate-center/driver-audit',
                                meta:{
                                    icon:"InfoFilled",
                                    title:'司机资料',
                                    activeMenu:'0-1',
                                    noCache:false,
                                },
                                index:'0-1',
                                children:[],
                            },
                            {
                                path:'car-info',
                                name:'CarInfo',
                                component:'khy-operate-center/car-info',
                                meta:{
                                    icon:"Van",
                                    title:'车辆资料',
                                    activeMenu:'0-2',
                                    noCache:false,
                                },
                                index:'0-2',
                                children:[],
                            },
                            {
                                path:'contract-manage',
                                name:'ContractManage',
                                component:'khy-operate-center/contract-manage',
                                meta:{
                                    icon:"Management",
                                    title:'合同管理',
                                    activeMenu:'0-3',
                                    noCache:false,
                                },
                                index:'0-3',
                                children:[],
                            },
                            {
                                path:'payee-info',
                                name:'PayeeInfo',
                                component:'khy-operate-center/payee-info',
                                meta:{
                                    icon:"Ticket",
                                    title:'收款账户',
                                    activeMenu:'0-4',
                                    noCache:false,
                                },
                                index:'0-4',
                                children:[],
                            },
                            {
                                path:'pay-detail',
                                name:'PayDetail',
                                component:'khy-operate-center/pay-detail',
                                meta:{
                                    icon:"Ticket",
                                    title:'支付明细表',
                                    activeMenu:'0-5',
                                    noCache:false,
                                },
                                index:'0-5',
                                children:[],
                            },
                        ]
                    },
                    {
                        path:'/income',
                        name:'Income',
                        component: "layout",
                        redirect: "noRedirect",
                        meta:{
                            icon:'Money',
                            title:'财务管理',
                        },
                        index:'1-0',
                        // 当子路由作为根路由显示时 path要和父菜单一致 在底层做了处理 动态路由子菜单path统一不带斜杠
                        alwaysShow:false,
                        hidden:false,
                        children: [
                            {
                                path:'income',
                                name:'AskIncome',
                                component: 'income',
                                meta:{
                                    icon:'Money',
                                    title: '咨询收入流水',
                                    activeMenu:'1-0',
                                },
                                index:'1-0',
                                children:[],
                            }
                        ]
                    },
                    {
                        path:'/dashboard',
                        name:'Dashboard',
                        component:'layout',
                        redirect:'noRedirect',
                        meta:{
                            icon:'Histogram',
                            title:'仪表盘',
                        },
                        index:'2-0',
                        // 即使子路由只有一个 也会嵌套显示
                        alwaysShow:true,
                        hidden:false,
                        children:[
                            {
                                path:'workplace',
                                name:'Workplace',
                                component:'dashboard/workplace',
                                meta:{
                                    icon:"TrendCharts",
                                    title:'工作台',
                                    activeMenu:'2-0'
                                },
                                index:'2-0',
                                children:[],
                            },
                            {
                                path:'workdemo',
                                name:'Workdemo',
                                component:'dashboard/workdemo',
                                meta:{
                                    icon:"TrendCharts",
                                    title:'组件示例',
                                    activeMenu:'2-1'
                                },
                                index:'2-1',
                                children:[],
                            },
                            {
                                path:'work-form',
                                name:'FormDemo',
                                component:'dashboard/formdemo',
                                meta:{
                                    icon:"TrendCharts",
                                    title:'表单研究',
                                    activeMenu:'2-2'
                                },
                                index:'2-2',
                                children:[],
                            },
                            {
                                path:'qrcode',
                                name:'QrCode',
                                component:'dashboard/qrcode',
                                meta:{
                                    icon:"TrendCharts",
                                    title:'二维码生成',
                                    activeMenu:'2-3'
                                },
                                index:'2-3',
                                children:[],
                            },
                            {
                                path:'sign',
                                name:'Sign',
                                component:'dashboard/work-esign',
                                meta:{
                                    icon:"TrendCharts",
                                    title:'电子签名',
                                    activeMenu:'2-4'
                                },
                                index:'2-4',
                                children:[],
                            },
                            {
                                path:'sankey',
                                name:'Sankey',
                                component:'dashboard/work-sankey',
                                meta:{
                                    icon:"TrendCharts",
                                    title:'桑基图-能量流动图',
                                    activeMenu:'2-5'
                                },
                                index:'2-5',
                                children:[],
                            },
                            {
                                path:'canvas',
                                name:'Canvas',
                                component:'dashboard/work-canvas',
                                meta:{
                                    icon:"TrendCharts",
                                    title:'canvas练习',
                                    activeMenu:'2-6'
                                },
                                index:'2-6',
                                children:[],
                            },
                            {
                                path:'canvas-play',
                                name:'CanvasPlay',
                                component:'dashboard/work-canvas-play',
                                meta:{
                                    icon:"TrendCharts",
                                    title:'canvasPlayer',
                                    activeMenu:'2-7'
                                },
                                index:'2-7',
                                children:[],
                            },
                            {
                                path:'video-play',
                                name:'VideoPlay',
                                component:'dashboard/work-video-play',
                                meta:{
                                    icon:"TrendCharts",
                                    title:'视频播放器',
                                    activeMenu:'2-8'
                                },
                                index:'2-8',
                                children:[],
                            },
                            {
                                path:'work-news',
                                name:'WorkNews',
                                component:'dashboard/work-news',
                                meta:{
                                    icon:"TrendCharts",
                                    title:'新闻资讯列表',
                                    activeMenu:'2-9'
                                },
                                index:'2-9',
                                children:[],
                            },
                            {
                                path:'work-cell-merge',
                                name:'WorkCellMerge',
                                component:'dashboard/work-cell-merge',
                                meta:{
                                    icon:"TrendCharts",
                                    title:'合并单元格',
                                    activeMenu:'2-10'
                                },
                                index:'2-10',
                                children:[],
                            },
                            {
                                path:'water-flow',
                                name:'Water-flow',
                                component:'app-work/app-water-flow-dynamic',
                                meta:{
                                    icon:"TrendCharts",
                                    title:'纯图片瀑布流',
                                    activeMenu:'2-11'
                                },
                                index:'2-11',
                                children:[],
                            },
                            {
                                path:'water-flow-v2',
                                name:'Water-flow-v2',
                                component:'app-work/app-water-flow-dynamic-v2',
                                meta:{
                                    icon:"TrendCharts",
                                    title:'图片和内容瀑布流',
                                    activeMenu:'2-12'
                                },
                                index:'2-12',
                                children:[],
                            },
                            {
                                path:'rtmp-play',
                                name:'work-rtmp-play',
                                component:'dashboard/work-rtmp-play',
                                meta:{
                                    icon:"TrendCharts",
                                    title:'直播拉流',
                                    activeMenu:'2-13'
                                },
                                index:'2-13',
                                children:[],
                            },
                        ]
                    },
                    {
                        path:'/animate',
                        name:'Animate',
                        component:'layout',
                        redirect:'noRedirect',
                        meta:{
                            icon:'Flag',
                            title:'动画研究',
                        },
                        index:'3-0',
                        // 即使子路由只有一个 也会嵌套显示
                        alwaysShow:true,
                        hidden:false,
                        children:[
                            {
                                // 子菜单的路径(访问链接)： 父菜单路径 + / + 子菜单路径 所以有多级目录时子菜单路径填写无需添加/
                                path:'animate-border-line',
                                name:'BorderLine',
                                component:'animate/animate-border-line',
                                meta:{
                                    icon:"TrendCharts",
                                    title:'边框流动效果',
                                    activeMenu:'3-0'
                                },
                                index:'3-0',
                                children:[],
                            },
                        ]
                    },
                ];
                resolve(appMenus)
            })
        },
    }
}

export default permission
