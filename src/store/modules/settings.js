// 项目系统配置数据模型
const settings = {
    state:{
        documentTitle:'数字管理平台',
        tagsView:true,
    },
    mutations:{
        SET_DOCUMENT_TITLE:(state, documentTitle) => {
            state.documentTitle = documentTitle
        },

        SET_TAGS_VIEW:(state, tagsView) => {
            state.tagsView = tagsView
        }
    },
    actions:{
        /**
         * 更改网页标题
         * @param commit 和state具有相同实例和方法的对象
         * @param title
         * @returns {Promise<unknown>}
         * @constructor
         */
        UpdateDocumentTitle({ commit }, title) {
            return new Promise(resolve => {
                const documentTitle =  title && title.length > 0 ? `${title}-数字管理平台` : '数字管理平台'
                document.title = documentTitle
                commit('SET_DOCUMENT_TITLE', documentTitle)
                resolve(documentTitle)
            })
        }
    }
}

export default settings
