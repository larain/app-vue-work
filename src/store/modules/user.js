import { removeToken } from '@/utils/auth'
import Cookies from "js-cookie";
import {getUserInfo, logout} from "@/api/login";
// 定义model
const user = {
    // 定义全局变量
    state:{
        // 登录标记
        isLogin:false,
        // 用户token
        token: '',
        // 用户昵称
        name: '',
        // 用户头像
        avatar: '',
        // 用户介绍
        introduction:'',
        // 用户角色
        roles: [],
        // 用户菜单权限
        permissions: []
    },

    // Setter - mutation限制 必须同步执行(更改state状态值 只能通过提交mutation进行更改  唯一路径)
    mutations:{
        SET_IS_LOGIN:(state, token) => {
            state.isLogin = token
        },

        SET_TOKEN:(state, token) => {
            state.token = token
        },

        SET_NAME:(state, name) => {
            state.name = name
        },

        SET_AVATAR:(state, avatar) => {
            state.avatar = avatar
        },

        SET_ROLES:(state, roles) => {
            state.roles = roles
        },

        SET_PERMISSIONS:(state, permissions) => {
            state.permissions = permissions
        },
    },

    // 变更实体的方法：Action 函数接受一个与 store 实例具有相同方法和属性的 context 对象，
    // 因此你可以调用 context.commit 提交一个 mutation，或者通过 context.state 和 context.getters 来获取 state 和 getters。
    // Action 通过 store.dispatch 方法触发
    // Action 就不受约束！我们可以在 action 内部执行异步操作：
    actions: {
        /**
         * 获取用户信息 - 此处可以网络请求 action本身返回的就是promise 网络请求返回的也是promise
         * @param commit 提交变更
         * @param state
         * @constructor
         */
        GetUserInfo({ commit }) {
            return new Promise((resolve, reject) => {
                getUserInfo().then(res => {
                    console.log('快货运获取用户信息成功：', res)
                    const data = res && res.data ? res.data : {}
                    const { name } = data
                    commit('SET_NAME', name)
                    commit('SET_AVATAR', 'https://img1.baidu.com/it/u=110021325,3211326711&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=500')
                    resolve(user.state)
                }).catch(err => {
                    console.log('快货运获取用户信息失败：', err)

                    // mock
                    // const { name } = {name:'QiuQiu'}
                    // commit('SET_NAME', name)
                    // commit('SET_AVATAR', 'https://img1.baidu.com/it/u=110021325,3211326711&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=500')
                    resolve(user.state)

                }).catch(err => {
                    reject(err)
                })
            })
        },

        LogOut({ commit }) {
            return new Promise((resolve, reject) => {
                logout().then(res => {
                    // 用户标记
                    removeToken('token')
                    commit('SET_TOKEN', '')
                    commit('SET_NAME', "")
                    commit('SET_IS_LOGIN', false)

                    // 路由相关
                    commit('SET_SIDEBAR_ROUTES', [])
                    commit('SET_STATIC_ROUTES', [])
                    commit('SET_DYNAMIC_ROUTES', [])
                    commit('SET_ACTIVE_MENU', '0-0')

                    resolve(user.state)
                }).catch(err => {
                    reject(err)
                })
            })
        },

        LogoutSuccess({ commit }) {
            removeToken('token')
            commit('SET_TOKEN', '')
            commit('SET_NAME', "")
            commit('SET_IS_LOGIN', false)

            // 路由相关
            commit('SET_SIDEBAR_ROUTES', [])
            commit('SET_STATIC_ROUTES', [])
            commit('SET_DYNAMIC_ROUTES', [])
            commit('SET_ACTIVE_MENU', '0-0')
        }
    }
}

// 导出
export default user
