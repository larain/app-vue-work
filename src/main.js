window.baseUrl = 'www.baidu.com'

// 从vue中引入创建项目实例的方法
import { createApp } from 'vue'

import print from 'vue3-print-nb'

// 全局公共样式 比如路由加载动画
import '@/assets/styles/index.scss' // global css
import '@/assets/styles/public.scss'
import '@/assets/styles/app.scss' // app css

// 引入路由
import router from "@/router";

// 引入状态管理器
import store from '@/store'

// 引入组件
import App from './App.vue'
import AppLogin from './components/AppLogin';
// 手机端签名界面
import AppSign from '@/views/app-work/app-sign';
// 手机端瀑布流
import AppWaterFlow from '@/views/app-work/app-water-flow-dynamic';
// 官网首页
import WebSite from '@/views/site/index'


// web端 - elementPlus - UI
import elementPlus from 'element-plus'
import 'element-plus/theme-chalk/index.css';

// 按需全局引入组件 - vantUI部分
import { Button } from "vant";
import { Icon } from 'vant';
import { Image as VanImage } from 'vant';
import { Lazyload } from 'vant';
import { Loading } from 'vant';
// 下拉刷新组件
import { PullRefresh } from 'vant';
// 表单组件引入
import { Form, Field, List, Cell, CellGroup, NavBar }  from "vant";

// 2. 引入组件样式
import 'vant/lib/index.css';

// Toast - 函数式组件 不是全局引入 但是可以在项目入口文件引入样式文件 避免重复引入
import 'vant/es/toast/style';

// Dialog - 函数式组件
import 'vant/es/dialog/style';

// Notify - 函数式组件
import 'vant/es/notify/style';

// ImagePreview - 函数式组件
import 'vant/es/image-preview/style';

// 解决 ElTable 自动宽度高度导致的「ResizeObserver loop limit exceeded」问题
const debounce = (fn, delay) => {
    let timer = null;
    return function () {
        let context = this;
        let args = arguments;
        clearTimeout(timer);
        timer = setTimeout(function () {
            fn.apply(context, args);
        }, delay);
    }
}

const _ResizeObserver = window.ResizeObserver;
window.ResizeObserver = class ResizeObserver extends _ResizeObserver{
    constructor(callback) {
        callback = debounce(callback, 16);
        super(callback);
    }
}

// 初始化一个全局实例:参数为首页 当前js文件为项目入口文件
// Vue 3.0版本以后 可以理解为所有组件均以此单例app去注册调用 然后外部直接使用组件 而不是像2.0变成this的属性
let configs = window.configs;
console.log('configs:', configs);

export let app;
let a = configs ? configs.isWeb : true;
console.log('configs isWeb:', a);
if (a) {
    app = createApp(App)
    // app = createApp(WebSite);
} else {
    app = createApp(AppWaterFlow);
}

// 模态框对象 全局挂载实例对象 这样可以访问实例对象下的所有导出方法
import { modal, tab } from '@/plugins'
app.config.globalProperties.$modal = modal;
app.config.globalProperties.$tab = tab;

// 全局挂载通用方法
import { resetForm, addDateRange, getResourceBaseURL } from './utils/util'
import { download, downloadFileBy } from "@/utils/request";

app.config.globalProperties.getFileBaseUrl = getResourceBaseURL;
app.config.globalProperties.resetForm = resetForm;
app.config.globalProperties.addDateRange = addDateRange;
app.config.globalProperties.download = download;
app.config.globalProperties.downloadFileBy = downloadFileBy;
// 测试全局变量写法
app.config.globalProperties.baseTag = 111;

// 全局挂载组件
// 页面垂直布局容器
import PageView from "@/components/Page/page-view";
// 分页组件
import Pagination from "@/components/Pagination";
// 图片预览组件
import ImagePreview from "@/components/ImagePreview";
import Chart from '@/components/Chart';
// svg component
import SvgIcon from '@/components/SvgIcon';
app.component('PageView', PageView)
app.component('Pagination', Pagination)
app.component('Chart', Chart)
app.component('ImagePreview', ImagePreview)
app.component('svg-icon', SvgIcon)

// 导入本地所以的svg图标
import '@/assets/icons/index';

// 注册需要的组件：在 app 上全局注册组件
app.use(Icon)
app.use(Button)
app.use(Lazyload)
app.use(VanImage)
app.use(Loading)
app.use(PullRefresh);
// 注册表单组件
app.use(Form)
app.use(Field)
app.use(List);
app.use(Cell)
app.use(CellGroup)
app.use(NavBar)

// element - UI部分(element组件以中文显示)
import locale from 'element-plus/lib/locale/lang/zh-cn'
app.use(elementPlus, {locale})

// 注册路由
app.use(router)

// 注册状态管理器
app.use(store)

// 打印
app.use(print)

// main.js中
// import VideoPlayer from 'vue-video-player'
// import "video.js/dist/video-js.css";
// app.use(VideoPlayer)


// main.js中注册element-plus所有icon 在需要用到图标资源的地方按需引入
import * as ElIcon from '@element-plus/icons-vue'
for (let iconName in ElIcon){
    // 将每一个图标注册为组件 后续动态赋值即可根据componet调用
    app.component(iconName, ElIcon[iconName])
}

// 整个生命周期只调用一次 将根实例挂载到dom上
app.mount('#app')

router.afterEach((to, from) => {
    window.myRouter = router;
    // 在这里可以访问路由实例
    console.log('在这里可以访问路由实例:', router);
})





