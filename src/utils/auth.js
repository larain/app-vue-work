// 专门用于处理cookie鉴权的文件
import Cookies from 'js-cookie'

// 自定义token 对应的key
const TokenKey = 'token'

export function getToken() {
  return Cookies.get(TokenKey)
}

export function setToken(token) {
  return Cookies.set(TokenKey, token)
}

export function removeToken() {
  return Cookies.remove(TokenKey)
}
