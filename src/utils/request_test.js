// 当基地址路径比较统一时 使用webpack打包 此时可以使用此网络请求类
import axios from 'axios'
// 默认错误码
import errorCode from './errorCode'
// HUD提示
import { ElMessage, ElLoading } from 'element-plus'
import {showMsg} from "@/components/MessageBox";

import { tansParams, blobValidate } from "@/util/util";

// 文件保存 - 可以指定文件保存名称
import { saveAs } from 'file-saver'
let downloadLoadingInstance;

// 服务端接收数据方式：默认为表单
axios.defaults.headers['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8'

// 创建axios实例
const service = axios.create({
  // axios中请求配置有baseURL选项，表示请求URL公共部分(如果项目的基地址比较统一 使用此种方式可以 但是多基地址情况 就不适应了)
  baseURL: process.env.VUE_APP_BASE_TEST_API,
  // 超时
  timeout: 50000,
  withCredentials:true,
  responseType: 'blob',
})

// 请求拦截器 对请求前的工作做处理
service.interceptors.request.use(config => {
  console.log('请求拦截器', config)
  if (config.method === 'get' && config.params) {
    let url = config.url + '?' + tansParams(config.params);
    url = url.slice(0, -1);
    config.params = {};
    config.url = url;
  }
  // showMsg('传入的文字',(close)=>{
  //   close()
  // })
  return config
})

// 请求响应器 对请求结果进行处理
service.interceptors.response.use(res => {
  console.log('请求响应拦截器', res)
  // 未设置状态码则默认成功状态
  const code = res.data.code || 0;
  console.log('code', code)
  // 获取错误信息
  const msg = errorCode[code] || res.data.msg || res.data.message || errorCode['default']
  // 二进制数据则直接返回(文件下载)
  if(res.request.responseType ===  'blob' || res.request.responseType ===  'arraybuffer') {
    return res.data
  }

  // code非0即为不成功
  if (code > 0 && code != 200) {
    // 弹层提示
    // ElMessage({
    //   message:msg,
    //   type:'warning'
    // })
    return Promise.reject({ code, msg })
  }

  // code为0成功
  return res.data
}, error => {
  // 请求异常处理
  return Promise.reject(error)
})

/**
 * 下载方法 - post
 * @param url 文件路径 请求拦截后可以传文件基础部分
 * @param params 下载文件参数
 * @param filename 下载后保存在本地的文件名称
 * @returns {Promise<axios.AxiosResponse<any>>}
 */
export function download(url, params, filename) {
  // element-ui 加载菊花
  downloadLoadingInstance = ElLoading.service({ text: "正在下载数据，请稍候", spinner: "el-icon-loading", background: "rgba(0, 0, 0, 0.7)", })
  return service.post(url, params, {
    transformRequest: [(params) => { return tansParams(params) }],
    headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
    responseType: 'blob'
  }).then(async (data) => {
    const isLogin = await blobValidate(data);
    if (isLogin) {
      // 为二进制数据
      const blob = new Blob([data])
      saveAs(blob, filename)
    } else {
      const resText = await data.text();
      const rspObj = JSON.parse(resText);
      const errMsg = errorCode[rspObj.code] || rspObj.msg || errorCode['default']
      ElMessage.error(errMsg);
    }
    downloadLoadingInstance.close();
  }).catch((r) => {
    console.error(r)
    ElMessage.error('下载文件出现错误，请联系管理员！')
    downloadLoadingInstance.close();
  })
}

/**
 * 下载文件方法 - get方式
 * @param url 文件路径(当未配置基地址时 可以传完整路径)
 * @param params 下载文件的参数 参数拼接在url后面
 * @param filename 下载后保存在本地的文件名称
 */
export function downloadFileBy(url, params, filename) {
  downloadLoadingInstance = ElLoading.service({ text: "正在下载数据，请稍候", spinner: "el-icon-loading", background: "rgba(0, 0, 0, 0.7)", })
  let finalUrl = url
  if (params && Object.keys(params).length > 0) {
    finalUrl = url + '?' + tansParams(url);
    finalUrl = finalUrl.slice(0, -1);
  }
  return service.get(finalUrl, {
    responseType: 'blob'
  }).then(async (data) => {
    const isLogin = await blobValidate(data);
    if (isLogin) {
      // 为二进制数据
      const blob = new Blob([data])
      saveAs(blob, filename)
    } else {
      const resText = await data.text();
      const rspObj = JSON.parse(resText);
      const errMsg = errorCode[rspObj.code] || rspObj.msg || errorCode['default']
      ElMessage.error(errMsg);
    }
    downloadLoadingInstance.close();
  }).catch((r) => {
    console.error(r)
    ElMessage.error('下载文件出现错误，请联系管理员！')
    downloadLoadingInstance.close();
  });
}

export default service
