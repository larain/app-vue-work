// 当基地址路径比较统一时 使用webpack打包 此时可以使用此网络请求类
import {baseUrl} from '../../public/app_config'
console.log('baseUrl:', baseUrl);

// 一定的时机在外部js文件中读取当前路由对象
// setTimeout(()=> {
//   let temp = window.myRouter.currentRoute;
//   console.log('currentRoute:', temp);
// }, 3000)


import axios from 'axios'
// 默认错误码
import errorCode from './errorCode'
import { getToken } from "@/utils/auth";
// HUD提示
import { ElMessage, ElLoading, ElMessageBox } from 'element-plus'
import {showMsg} from "@/components/MessageBox";

// 状态管理器
import store from "@/store";

import {tansParams, blobValidate, getStorage} from "@/utils/util";

// 是否显示重新登录
export let isRelogin = { show: false };

// 文件保存 - 可以指定文件保存名称
import { saveAs } from 'file-saver'
import Cookies from "js-cookie";
let downloadLoadingInstance;

// 接口请正常code值
let rightCode = '00000';
// token已经失效的code值
let needLoginCode = 'RBAC000';

// 服务端接收数据方式：默认为表单
axios.defaults.headers['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8'

// 创建axios实例
const service = axios.create({
  // axios中请求配置有baseURL选项，表示请求URL公共部分(如果项目的基地址比较统一 使用此种方式可以 但是多基地址情况 就不适应了)
  baseURL: process.env.VUE_APP_BASE_API,
  // baseURL:baseUrl,
  // 超时
  timeout: 50000,
  withCredentials:true,
})

// 请求拦截器 对请求前的工作做处理
service.interceptors.request.use(config => {
  console.log('请求拦截器', config)
  if (config.method === 'get' && config.params) {
    let url = config.url + '?' + tansParams(config.params);
    url = url.slice(0, -1);
    config.params = {};
    config.url = url;
  }

  // 认证信息：如果没有 则会弹出登录框 解密后内容：test_client:test_secret
  // 未登录时

  // 认证类型
  const token =  getToken('token')
  console.log('token:', token)
  if (token) {
    // 登录后报文形式为json
    config.headers['Content-Type'] = 'application/json;charset=utf-8'
  } else {
    // 未登录报文形式为表单
    config.headers['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8'
  }

  let tokenType = 'Basic'
  // 默认的token 可能每个用户不一样
  let access_token = 'dGVzdF9jbGllbnQ6dGVzdF9zZWNyZXQ='
  if (token) {
    access_token = getStorage('access_token').result
    tokenType = getStorage('tokenType').result
    if (tokenType == 'bearer') {
      tokenType = 'Bearer'
    }
    if (!access_token) {
      console.log('access_token已失效')
      if (!isRelogin.show) {
        isRelogin.show = true;
        ElMessageBox.confirm('登录状态已过期，请重新登录', '系统提示', {
              confirmButtonText: '重新登录',
              cancelButtonText: '',
              type: 'warning',
              // 不显示右上角关闭按钮
              showClose:false,
              // 不显示取消按钮
              showCancelButton:false,
              // 点击遮罩层不关闭弹窗
              closeOnClickModal:false,
            }
        ).then(() => {
          isRelogin.show = false;
          store.dispatch('LogOut').then(() => {

          }).catch(err => {
            // store.dispatch('LogoutSuccess')
            // location.href = '/index';
          })

          store.dispatch('LogoutSuccess')
          location.href = '/index';
        }).catch(() => {
          isRelogin.show = false;
        });
      }
    }
  }

  // 让每个请求携带 自定义token 请根据实际情况自行修改(此值无的时候 浏览器会弹出账号密码输入框)
  config.headers['Authorization'] = tokenType + ' ' + access_token

  // showMsg('传入的文字啊啊啊',(close)=>{
  //   close()
  // })

  return config
})

// 请求响应器 对请求结果进行处理
service.interceptors.response.use(res => {
  console.log('请求响应拦截器', res)
  // 未设置状态码则默认成功状态
  const code = res.data.code || 0;
  console.log('code', code)
  // 获取错误信息
  const msg = errorCode[code] || res.data.msg || res.data.message || errorCode['default'];
  // 二进制数据则直接返回(文件下载)
  if(res.request.responseType ===  'blob' || res.request.responseType ===  'arraybuffer') {
    return res.data;
  }

  // token失效
  if (code == needLoginCode) {
    reLogin();
    return;
  }

  // code非0即为不成功
  if ((code > 0 && code != 200) || code != rightCode) {
    // 弹层提示
    ElMessage({
      message:msg,
      type:'warning'
    })
    return Promise.reject({ code, msg })
  }

  // code为0成功
  return res.data
}, error => {
  // 请求异常处理
  return Promise.reject(error)
})

/**
 * token已经失效 重新登录
 */
function reLogin() {
  if (!isRelogin.show) {
    isRelogin.show = true;
    ElMessageBox.confirm('登录状态已过期，请重新登录', '系统提示', {
          confirmButtonText: '重新登录',
          cancelButtonText: '',
          type: 'warning',
          // 不显示右上角关闭按钮
          showClose:false,
          // 不显示取消按钮
          showCancelButton:false,
          // 点击遮罩层不关闭弹窗
          closeOnClickModal:false,
        }
    ).then(() => {
      isRelogin.show = false;
      store.dispatch('LogOut').then(() => {

      }).catch(err => {
        // store.dispatch('LogoutSuccess')
        // location.href = '/index';
      })
      store.dispatch('LogoutSuccess')
      location.href = '/index';
    }).catch(() => {
      isRelogin.show = false;
    });
  }
}

/**
 * 下载方法 - post
 * @param url 文件路径 请求拦截后可以传文件基础部分
 * @param params 下载文件参数
 * @param filename 下载后保存在本地的文件名称
 * @returns {Promise<axios.AxiosResponse<any>>}
 */
export function download(url, params, filename) {
  // element-ui 加载菊花
  downloadLoadingInstance = ElLoading.service({ text: "正在下载数据，请稍候", spinner: "el-icon-loading", background: "rgba(0, 0, 0, 0.7)", })
  return service.post(url, params, {
    transformRequest: [(params) => { return tansParams(params) }],
    headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
    responseType: 'blob'
  }).then(async (data) => {
    const isLogin = await blobValidate(data);
    if (isLogin) {
      // 为二进制数据
      const blob = new Blob([data])
      saveAs(blob, filename)
    } else {
      const resText = await data.text();
      const rspObj = JSON.parse(resText);
      const errMsg = errorCode[rspObj.code] || rspObj.msg || errorCode['default']
      ElMessage.error(errMsg);
    }
    downloadLoadingInstance.close();
  }).catch((r) => {
    console.error(r)
    ElMessage.error('下载文件出现错误，请联系管理员！')
    downloadLoadingInstance.close();
  })
}

/**
 * 下载文件方法 - get方式
 * @param url 文件路径(当未配置基地址时 可以传完整路径)
 * @param params 下载文件的参数 参数拼接在url后面
 * @param filename 下载后保存在本地的文件名称
 */
export function downloadFileBy(url, params, filename) {
  downloadLoadingInstance = ElLoading.service({ text: "正在下载数据，请稍候", spinner: "el-icon-loading", background: "rgba(0, 0, 0, 0.7)", })
  let finalUrl = url
  if (params && Object.keys(params).length > 0) {
    finalUrl = url + '?' + tansParams(url);
    finalUrl = finalUrl.slice(0, -1);
  }
  return service.get(finalUrl, {
    responseType: 'blob'
  }).then(async (data) => {
    const isLogin = await blobValidate(data);
    if (isLogin) {
      // 为二进制数据
      const blob = new Blob([data])
      saveAs(blob, filename)
    } else {
      const resText = await data.text();
      const rspObj = JSON.parse(resText);
      const errMsg = errorCode[rspObj.code] || rspObj.msg || errorCode['default']
      ElMessage.error(errMsg);
    }
    downloadLoadingInstance.close();
  }).catch((r) => {
    console.error(r)
    ElMessage.error('下载文件出现错误，请联系管理员！')
    downloadLoadingInstance.close();
  });
}

export default service
