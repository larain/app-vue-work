// 从vue中引入创建项目实例的方法
import { createApp } from 'vue'

// 引入组件
import App from './App.vue'

export const app = createApp(App)

// 整个生命周期只调用一次 将根实例挂载到dom上
app.mount('#app')
