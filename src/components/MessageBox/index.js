// MessageBox.js文件
import {createApp} from 'vue'
import MessageBox from "./index.vue"

// 引入element-ui组件及其样式
import ElementPlus from 'element-plus';
import 'element-plus/dist/index.css';

/**
 * 导出方法
 * @param msg
 * @param onClick
 */
export function showMsg(msg, onClick) {
  // 往body上增加一个标签
  const div = document.createElement('div');
  document.body.appendChild(div)
  // 渲染组件
  const app = createApp(MessageBox,{
    // 第二个参数就是传递的属性,
    msg,
    onClick() {
      // 组件内部点击方法
      onClick(() => {
        // 移除挂载
        app.unmount();
        // 移除标签
        div.remove()
      })
    }
  });
  // 在组件注入前注册element-ui 这样在自定义组件内就可以使用element-ui
  app.use(ElementPlus)
  app.mount(div)
}
