/**
 * 定义model基础类
 */
export class appBaseModel {
    /**
     * 关键数据id
     */
    dataId;
    /**
     * 信息源字典
     */
    infoDic;
    /**
     * 信息源数组
     */
    dataSource;

    constructor(args) {
        args = args ? args : {};
        this.infoDic = args
    }
}
