import {util} from "@/utils/util";
import {appBaseModel} from "@/components/appModel/appBaseModel";

/**
 * 当前登录用户
 * @type {null}
 */
let currentUser = null;

/**
 * 创建User单元数据模型
 */
export class appUser extends appBaseModel{
    // oa账号
    oaAccount;
    // oa密码
    oaPwd;
    // 设备唯一号
    uuid;
    // oa用户id
    userId;
    // oa用户名称
    userName;
    // oa层级id
    deptId;
    // 层级名称
    departName;
    // 层级对应的职位名称
    positionName;
    // oa用户头像
    headImg;
    // 对应的玄乐账号id
    xlUserId;
    // 对应的玄乐账号
    xlLoginName;
    // oA登录token
    oaLoginToken;

    constructor(args) {
        super();
        if (!currentUser) {
            // 当前用户信息不存在时初始化
            currentUser = this
        }
        args = args ? args : {};
        this.oaAccount = util.nullStr(args.oaAccount)
        this.oaPwd = util.nullStr(args.oaPwd)
        this.uuid = util.nullStr(args.uuid)
        this.userId = util.nullStr(args.userId)
        this.userName = util.nullStr(args.userName);
        this.deptId = util.nullStr(args.deptId)
        this.departName = util.nullStr(args.departName)
        this.positionName = util.nullStr(args.positionName);
        this.headImg = util.nullStr(args.headImg)
        this.xlUserId = util.nullStr(args.xlUserId);
        this.xlLoginName = util.nullStr(args.xlLoginName);
        this.oaLoginToken = util.nullStr(args.oaLoginToken);
        return currentUser;
    }
}

/**
 * 获取当前登录用户信息
 * @returns {null}
 */
export function getUserInfo() {
    if (!currentUser) {
        // 尝试从缓存中读取 已经登录情况下 界面初始化进来时
        const userInfo = util.getLocalStorage('userInfo')
        if (userInfo) {
            currentUser = new appUser(userInfo)
        }
    }
    return currentUser ? currentUser : {};
}

/**
 * 是否登录
 * @returns {boolean}
 */
export function isLogin() {
    return currentUser ? true : false
}
