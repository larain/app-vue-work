# app-vue-work

#### Description
基于Vue 3.x搭建的前端框架，主要针对的是移动端前端，集成VantUI框架，网络请求基于axios封装。目标是便于以后混合开发，有想学习前端技术的可以交流学习。

#### Software Architecture
Software architecture description

#### Installation

1.  下载node.js
2.  使用前端开发工具打开项目后，在终端允许npm install
3.  项目依赖包安装完成后，npm start运行项目

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
